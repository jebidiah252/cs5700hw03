﻿using System.IO;
using System.Runtime.Serialization.Json;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class SaveCommand : Command
    {
        private readonly string filename;
        internal SaveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                filename = commandParameters[0] as string;
        }

        public override void Execute()
        {
            if (filename == null)
                return;
            StreamWriter writer = new StreamWriter(filename);
            TargetDrawing?.SaveToStream(writer.BaseStream);
            writer.Close();
        }
    }
}