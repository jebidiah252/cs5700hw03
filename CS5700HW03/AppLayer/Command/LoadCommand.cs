﻿using System.IO;

namespace AppLayer.Command
{
    public class LoadCommand : Command
    {
        private readonly string filename;

        internal LoadCommand() { }
        internal LoadCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                filename = commandParameters[0] as string;
        }

        public override void Execute()
        {
            if (filename == null)
                return;
            StreamReader reader = new StreamReader(filename);
            TargetDrawing?.LoadFromStream(reader.BaseStream);
            reader.Close();
        }
    }
}