﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class ScaleCommand : Command
    {
        private Performer toBeScaled;
        private float newScale;
        internal ScaleCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                toBeScaled = commandParameters[0] as Performer;
            if (commandParameters.Length > 1)
                newScale = (float)commandParameters[1];
            
        }
        public override void Execute()
        {
            if (toBeScaled == null)
                return;
            if (toBeScaled.IsSelected)
            {
                try
                {

                    toBeScaled.Size = new Size
                    {
                        Width = Convert.ToInt16(Math.Round(toBeScaled.Size.Width * newScale, 0)),
                        Height = Convert.ToInt16(Math.Round(toBeScaled.Size.Height * newScale, 0))
                    };
                    TargetDrawing.IsDirty = true;
                }
                catch (Exception e)
                {

                }
            }
        }
    }
}
