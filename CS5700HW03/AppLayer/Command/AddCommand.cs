﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class AddCommand : Command
    {
        private const int NormalWidth = 80;
        private const int NormalHeight = 80;

        private readonly string performerType;
        private Point location;
        private readonly float scale;
        internal AddCommand() { }

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="commandParameters">An array of parameters, where
        ///     [1]: string     Performer type -- a fully qualified resource name
        ///     [2]: Point      center location for the Performer, defaut = top left corner
        ///     [3]: float      scale factor</param>
        internal AddCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                performerType = commandParameters[0] as string;

            if (commandParameters.Length > 1)
                location = (Point)commandParameters[1];
            else
                location = new Point(0, 0);

            if (commandParameters.Length > 2)
                scale = (float)commandParameters[2];
            else
                scale = 1.0F;
        }

        public override void Execute()
        {
            if (string.IsNullOrWhiteSpace(performerType) || TargetDrawing == null) return;

            Size performerSize = new Size()
            {
                Width = Convert.ToInt16(Math.Round(NormalWidth * scale, 0)),
                Height = Convert.ToInt16(Math.Round(NormalHeight * scale, 0))
            };
            Point performerLocation = new Point(location.X - performerSize.Width / 2, location.Y - performerSize.Height / 2);

            PerformerExtrinsicState extrinsicState = new PerformerExtrinsicState()
            {
                PerformerType = performerType,
                Location = performerLocation,
                Size = performerSize
            };
            var performer = TargetDrawing.Factory.GetPerformer(extrinsicState);
            
            TargetDrawing.Add(performer);
        }
    }
}