﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class MoveCommand : Command
    {
        private Performer toBeMoved;
        private Point newPoint;

        /// <summary>
        /// Constructor
        /// 
        /// </summary>
        /// <param name="commandParameters">An array of parameters, where
        ///     [1]: string     Performer type -- a fully qualified resource name
        ///     [2]: Point      center location for the Performer, defaut = top left corner
        ///     [3]: float      scale factor</param>
        internal MoveCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                toBeMoved = commandParameters[0] as Performer;
            if (commandParameters.Length > 1)
                newPoint.X = (int)commandParameters[1];
            if (commandParameters.Length > 2)
                newPoint.Y = (int)commandParameters[2];
        }

        public override void Execute()
        {
            if (toBeMoved == null)
                return;
            if (toBeMoved.IsSelected)
            {
                toBeMoved.Location = new Point(newPoint.X, newPoint.Y);
                TargetDrawing.IsDirty = true;
            }
        }
    }
}
