﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;

namespace AppLayer.Command
{
    public class Invoker
    {

        private Thread worker;
        private readonly Stack<Command> undoStack = new Stack<Command>();
        private readonly ConcurrentQueue<Command> todoQueue = new ConcurrentQueue<Command>();
        private bool keepGoing;
        private readonly AutoResetEvent enqueueOccurred = new AutoResetEvent(false);

        public void Start()
        {
            keepGoing = true;
            worker = new Thread(Run);
            worker.Start();
        }

        public void Stop()
        {
            keepGoing = false;
        }

        public void EnqueueCommandForExecution(Command cmd)
        {
            if (cmd != null)
            {
                // TODO: Place the cmd into the todoQueue
                todoQueue.Enqueue(cmd);
                // Raise signal indicating that something was placed into the queue.  This wakes up the other thread,
                // if it is waiting for the something to be placed into the queue
                enqueueOccurred.Set();
            }

        }

        public void Undo()
        {
            // TODO: Pop a command from the undoStack, and call Undo on that command
            Command cmd = undoStack.Pop();

        }

        private void Run()
        {
            while (keepGoing)
            {
                // TODO: Try to get a command out of the queue.
                //		 If you got a command,
                //				Execute the command.  Be sure that the execute method for the command
                //					saves the necessary state information for an undo 
                //				then Push it onto the undoStack.
                Command cmdToExecute;
                if(todoQueue.TryDequeue(out cmdToExecute))
                {
                    cmdToExecute.Execute();
                    undoStack.Push(cmdToExecute);
                }

                //		 Else there was no command in the queue, execute the following statement, which
                //				causes the thread to wait for up to 100 ms for something to be placed
                //				into the queue	
                else
                {
                    enqueueOccurred.WaitOne(100);
                }		
            }

        }
    }
}





