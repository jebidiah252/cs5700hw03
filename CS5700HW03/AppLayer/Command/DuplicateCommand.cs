﻿using System;
using System.Drawing;
using AppLayer.DrawingComponents;

namespace AppLayer.Command
{
    public class DuplicateCommand : Command
    {
        private readonly Performer toBeCopied;
        internal DuplicateCommand(params object[] commandParameters)
        {
            if (commandParameters.Length > 0)
                toBeCopied = commandParameters[0] as Performer;
        }
        public override void Execute()
        {
            if (toBeCopied == null)
                return;
            Performer newPerformer = toBeCopied;
            newPerformer.Location = new Point(toBeCopied.Location.X + 5, toBeCopied.Location.Y + 5);
            TargetDrawing.Add(newPerformer);
        }
    }
}
