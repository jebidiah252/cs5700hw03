﻿using System;
using System.Collections.Generic;

namespace AppLayer.DrawingComponents
{
    public class PerformerFactory
    {
        public string ResourceNamePattern { get; set; }
        public Type ReferenceType { get; set; }

        private readonly Dictionary<string, PerformerWithIntrinsicState> sharedPerformers = new Dictionary<string, PerformerWithIntrinsicState>();

        public PerformerWithAllState GetPerformer(PerformerExtrinsicState extrinsicState)
        {
            string resourceName = string.Format(ResourceNamePattern, extrinsicState.PerformerType);

            PerformerWithIntrinsicState performerWithIntrinsicState;
            if (sharedPerformers.ContainsKey(extrinsicState.PerformerType))
                performerWithIntrinsicState = sharedPerformers[extrinsicState.PerformerType];
            else
            {
                performerWithIntrinsicState = new PerformerWithIntrinsicState();
                performerWithIntrinsicState.LoadFromResource(resourceName, ReferenceType);
                sharedPerformers.Add(extrinsicState.PerformerType, performerWithIntrinsicState);
            }

            return new PerformerWithAllState(performerWithIntrinsicState, extrinsicState);
        }
    }
}