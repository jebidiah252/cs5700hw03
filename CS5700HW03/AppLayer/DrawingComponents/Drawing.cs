﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Json;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using AppLayer.Database;

namespace AppLayer.DrawingComponents
{
    public class Drawing
    {
        private static readonly DataContractJsonSerializer JsonSerializer = new DataContractJsonSerializer(typeof(List<PerformerExtrinsicState>));

        private readonly List<Performer> performers = new List<Performer>();

        private readonly object myLock = new object();

        public PerformerFactory Factory { get; set; }
        public Performer SelectedVar { get; set; }
        public bool IsDirty { get; set; }
        public int PerformerCount => performers.Count;

        public void Clear()
        {
            lock (myLock)
            {
                performers.Clear();
                IsDirty = true;
            }
        }

        public void Add(Performer performer)
        {
            if (performer != null)
            {
                lock (myLock)
                {
                    performers.Add(performer);
                    IsDirty = true;
                }
            }
        }

        public void RemovePerformer(Performer performer)
        {
            if (performer != null)
            {
                lock (myLock)
                {
                    if (SelectedVar == performer)
                        SelectedVar = null;
                    performers.Remove(performer);
                    IsDirty = true;
                }
            }
        }

        public Performer FindPerformerAtPosition(Point location)
        {
            Performer result;
            lock (myLock)
            {
                result = performers.FindLast(t => location.X >= t.Location.X &&
                                              location.X < t.Location.X + t.Size.Width &&
                                              location.Y >= t.Location.Y &&
                                              location.Y < t.Location.Y + t.Size.Height);
            }
            return result;
        }

        public void DeselectAll()
        {
            lock (myLock)
            {
                foreach (var t in performers)
                    t.IsSelected = false;
                IsDirty = true;
            }
        }

        public void DeleteAllSelected()
        {
            lock (myLock)
            {
                performers.RemoveAll(t => t.IsSelected);
                IsDirty = true;
            }
        }

        public bool Draw(Graphics graphics)
        {
            bool didARedraw = false;
            lock (myLock)
            {
                if (IsDirty)
                {
                    graphics.Clear(Color.White);
                    foreach (var t in performers)
                        t.Draw(graphics);
                    IsDirty = false;
                    didARedraw = true;
                }
            }
            return didARedraw;
        }

        public void LoadFromStream(Stream stream)
        {
            var extrinsicStates = JsonSerializer.ReadObject(stream) as List<PerformerExtrinsicState>;
            if (extrinsicStates == null) return;

            lock (myLock)
            {
                performers.Clear();
                foreach (PerformerExtrinsicState extrinsicState in extrinsicStates)
                {
                    Performer performer = Factory.GetPerformer(extrinsicState);
                    performers.Add(performer);
                }
                IsDirty = true;
            }
        }

        public void SaveToStream(Stream stream)
        {
            List<PerformerExtrinsicState> extrinsicStates = new List<PerformerExtrinsicState>();
            lock (myLock)
            {
                foreach (Performer performer in performers)
                {
                    PerformerWithAllState t = performer as PerformerWithAllState;
                    if (t != null)
                        extrinsicStates.Add(t.ExtrinsicStatic);
                }
            }
            JsonSerializer.WriteObject(stream, extrinsicStates);
        }

    }
}