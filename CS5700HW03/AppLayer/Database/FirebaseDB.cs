﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using Firebase.Database;
using Firebase.Database.Query;
using Newtonsoft.Json;
using AppLayer.DrawingComponents;

namespace AppLayer.Database
{
    public class FirebaseDB
    {
        private static FirebaseDB instance;
        private FirebaseClient client;

        public static FirebaseDB Instance => instance ?? (instance = new FirebaseDB());

        private FirebaseDB()
        {
            client = new FirebaseClient("https://cs5700hw03.firebaseio.com/");
        }

        public async void Save(List<PerformerExtrinsicState> extrinsicStates)
        {
            var res = await client.Child("pictures").PostAsync(extrinsicStates);
            Debug.WriteLine(res.Key);
        }
    }
}
