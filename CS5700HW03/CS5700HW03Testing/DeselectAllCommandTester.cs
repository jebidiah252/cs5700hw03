﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer.Command;
using AppLayer.DrawingComponents;

namespace CS5700HW03Testing
{
    [TestClass]
    public class DeselectAllCommandTester
    {
        [TestMethod]
        public void DeselectCommand_NonEmptyDrawing()
        {
            // Setup a drawing
            PerformerFactory performerfactory = new PerformerFactory() { ResourceNamePattern = "CS5700HW03Testing.Graphics.{0}.png", ReferenceType = typeof(NewCommandTester) };
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.Add(performerfactory.GetPerformer(new PerformerExtrinsicState() { PerformerType = "Singer-01", Location = new Point(10, 10), Size = new Size(80, 80), IsSelected = true }));
            drawing.Add(performerfactory.GetPerformer(new PerformerExtrinsicState() { PerformerType = "Singer-01", Location = new Point(200, 310), Size = new Size(80, 80), IsSelected = true }));
            drawing.Add(performerfactory.GetPerformer(new PerformerExtrinsicState() { PerformerType = "Singer-01", Location = new Point(240, 150), Size = new Size(80, 80), IsSelected = true }));
            drawing.Add(performerfactory.GetPerformer(new PerformerExtrinsicState() { PerformerType = "Singer-01", Location = new Point(350, 300), Size = new Size(80, 80), IsSelected = true }));
            Assert.AreEqual(4, drawing.PerformerCount);

            // setup a New command
            Command newCmd = commandFactory.Create("deselect");
            Assert.IsNotNull(newCmd);

            // Stimulate (Execute newCmd.Execute)
            newCmd.Execute();

            // Assert the predicated results
            Assert.AreEqual(4, drawing.PerformerCount);
        }

        [TestMethod]
        public void DeselectCommand_EmptyDrawing()
        {
            Drawing drawing = new Drawing();
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = drawing };

            Assert.AreEqual(0, drawing.PerformerCount);

            Command newCmd = commandFactory.Create("Deselect");
            Assert.IsNotNull(newCmd);
            newCmd.Execute();
            Assert.AreEqual(0, drawing.PerformerCount);

        }

        [TestMethod]
        public void DeselectCommand_NoDrawing()
        {
            CommandFactory commandFactory = new CommandFactory() { TargetDrawing = null };

            Command newCmd = commandFactory.Create("Deselect");
            Assert.IsNotNull(newCmd);
            newCmd.Execute();
            // This didn't throw an exception, then it worked as expected
        }
    }
}
