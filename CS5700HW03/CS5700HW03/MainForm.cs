﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

using AppLayer.DrawingComponents;
using AppLayer.Command;

namespace CS5700HW03
{
    public partial class MainForm : Form
    {
        private readonly Drawing drawing;
        private readonly CommandFactory commandFactory;
        private string currentTreeResource;
        private float currentScale = 1;
        private Performer lastKnown;
        private bool isPerformerSelected = false;

        private enum PossibleModes
        {
            None,
            PerformerDrawing,
            Selection
        };

        private PossibleModes mode = PossibleModes.None;

        private Bitmap imageBuffer;
        private Graphics imageBufferGraphics;
        private Graphics panelGraphics;

        public MainForm()
        {
            InitializeComponent();

            drawing = new Drawing();
            commandFactory = new CommandFactory() { TargetDrawing = drawing };
            drawing.Factory = new PerformerFactory()
            {
                ResourceNamePattern = @"CS5700HW03.Graphics.{0}.png",
                ReferenceType = typeof(Program)
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
            refreshTimer.Start();
        }

        private void ComputeDrawingPanelSize()
        {
            int width = Width - drawingToolStrip.Width;
            int height = Height - fileToolStrip.Height;

            drawingPanel.Size = new Size(width, height);
            drawingPanel.Location = new Point(drawingToolStrip.Width, fileToolStrip.Height);

            imageBuffer = null;
            if (drawing != null)
                drawing.IsDirty = true;
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            DisplayDrawing();
        }

        private void DisplayDrawing()
        {
            if (imageBuffer == null)
            {
                imageBuffer = new Bitmap(drawingPanel.Width, drawingPanel.Height);
                imageBufferGraphics = Graphics.FromImage(imageBuffer);
                panelGraphics = drawingPanel.CreateGraphics();
            }

            if (drawing.Draw(imageBufferGraphics))
                panelGraphics.DrawImageUnscaled(imageBuffer, 0, 0);
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            commandFactory.Create("new").Execute();
        }

        private void ClearOtherSelectedTools(ToolStripButton ignoreItem)
        {
            foreach (ToolStripItem item in drawingToolStrip.Items)
            {
                ToolStripButton toolButton = item as ToolStripButton;
                if (toolButton != null && item != ignoreItem && toolButton.Checked)
                    toolButton.Checked = false;
            }
        }

        private void pointerButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
            {
                mode = PossibleModes.Selection;
                currentTreeResource = string.Empty;
            }
            else
            {
                commandFactory.Create("deselect").Execute();
                mode = PossibleModes.None;
                isPerformerSelected = false;
                lastKnown = null;
            }
        }

        private void performerButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            ClearOtherSelectedTools(button);

            commandFactory.Create("deselect").Execute();
            isPerformerSelected = false;
            lastKnown = null;

            if (button != null && button.Checked)
                currentTreeResource = button.Text;
            else
                currentTreeResource = string.Empty;

            mode = (currentTreeResource != string.Empty) ? PossibleModes.PerformerDrawing : PossibleModes.None;
        }

        private void drawingPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (mode == PossibleModes.PerformerDrawing)
            {
                if (!string.IsNullOrWhiteSpace(currentTreeResource))
                    commandFactory.Create("add", currentTreeResource, e.Location, currentScale)
                        .Execute();
            }
            else if (mode == PossibleModes.Selection)
            {
                if (lastKnown == null && isPerformerSelected == false)
                {
                    lastKnown = drawing.FindPerformerAtPosition(e.Location);
                    commandFactory.Create("select", e.Location).Execute();
                    isPerformerSelected = true;
                }
                else
                    commandFactory.Create("move", lastKnown, e.X, e.Y).Execute();
            }            
        }

        private void scale_Leave(object sender, EventArgs e)
        {
            currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
            scale.Text = currentScale.ToString(CultureInfo.InvariantCulture);

            if (lastKnown != null && isPerformerSelected)
            {
                commandFactory.Create("scale", lastKnown, currentScale).Execute();
            }
        }

        private float ConvertToFloat(string text, float min, float max, float defaultValue)
        {
            float result = defaultValue;
            if (!string.IsNullOrWhiteSpace(text))
            {
                if (!float.TryParse(text, out result))
                    result = defaultValue;
                else
                    result = Math.Max(min, Math.Min(max, result));
            }
            return result;
        }

        private void scale_TextChanged(object sender, EventArgs e)
        {
            currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                DefaultExt = "json",
                Multiselect = false,
                RestoreDirectory = true,
                Filter = @"JSON files (*.json)|*.json|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                commandFactory.Create("load", dialog.FileName).Execute();
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                DefaultExt = "json",
                RestoreDirectory = true,
                Filter = @"JSON files (*.json)|*.json|All files (*.*)|*.*"
            };

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                commandFactory.Create("save", dialog.FileName).Execute();
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            ComputeDrawingPanelSize();
        }

        private void scale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (lastKnown != null && isPerformerSelected)
                {
                    commandFactory.Create("scale", lastKnown, currentScale).Execute();
                }
            }
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            if (lastKnown != null && isPerformerSelected)
            {
                commandFactory.Create("duplicate", lastKnown).Execute();
            }
        }
    }
}
