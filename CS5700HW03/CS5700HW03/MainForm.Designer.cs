﻿namespace CS5700HW03
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.fileToolStrip = new System.Windows.Forms.ToolStrip();
            this.newButton = new System.Windows.Forms.ToolStripButton();
            this.openButton = new System.Windows.Forms.ToolStripButton();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.drawingToolStrip = new System.Windows.Forms.ToolStrip();
            this.pointerButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.scaleLabel = new System.Windows.Forms.ToolStripLabel();
            this.scale = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.singer01Button = new System.Windows.Forms.ToolStripButton();
            this.singer02Button = new System.Windows.Forms.ToolStripButton();
            this.guitarist01Button = new System.Windows.Forms.ToolStripButton();
            this.guitarist02Button = new System.Windows.Forms.ToolStripButton();
            this.bassist01Button = new System.Windows.Forms.ToolStripButton();
            this.bassist02Button = new System.Windows.Forms.ToolStripButton();
            this.keys01Button = new System.Windows.Forms.ToolStripButton();
            this.keys02Button = new System.Windows.Forms.ToolStripButton();
            this.drums01Button = new System.Windows.Forms.ToolStripButton();
            this.drums02Button = new System.Windows.Forms.ToolStripButton();
            this.drawingPanel = new System.Windows.Forms.Panel();
            this.refreshTimer = new System.Windows.Forms.Timer(this.components);
            this.copyButton = new System.Windows.Forms.ToolStripButton();
            this.fileToolStrip.SuspendLayout();
            this.drawingToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileToolStrip
            // 
            this.fileToolStrip.AutoSize = false;
            this.fileToolStrip.BackColor = System.Drawing.Color.CadetBlue;
            this.fileToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.fileToolStrip.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.fileToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newButton,
            this.openButton,
            this.saveButton,
            this.copyButton});
            this.fileToolStrip.Location = new System.Drawing.Point(0, 0);
            this.fileToolStrip.Name = "fileToolStrip";
            this.fileToolStrip.Size = new System.Drawing.Size(914, 64);
            this.fileToolStrip.TabIndex = 2;
            this.fileToolStrip.Text = "toolStrip1";
            // 
            // newButton
            // 
            this.newButton.AutoSize = false;
            this.newButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newButton.Image = ((System.Drawing.Image)(resources.GetObject("newButton.Image")));
            this.newButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(61, 61);
            this.newButton.Text = "New";
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // openButton
            // 
            this.openButton.AutoSize = false;
            this.openButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openButton.Image = ((System.Drawing.Image)(resources.GetObject("openButton.Image")));
            this.openButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(61, 61);
            this.openButton.Text = "Open";
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.AutoSize = false;
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = ((System.Drawing.Image)(resources.GetObject("saveButton.Image")));
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(61, 61);
            this.saveButton.Text = "Save";
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // drawingToolStrip
            // 
            this.drawingToolStrip.AutoSize = false;
            this.drawingToolStrip.BackColor = System.Drawing.Color.PowderBlue;
            this.drawingToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.drawingToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.drawingToolStrip.ImageScalingSize = new System.Drawing.Size(64, 64);
            this.drawingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pointerButton,
            this.toolStripSeparator1,
            this.scaleLabel,
            this.scale,
            this.toolStripSeparator2,
            this.singer01Button,
            this.singer02Button,
            this.guitarist01Button,
            this.guitarist02Button,
            this.bassist01Button,
            this.bassist02Button,
            this.keys01Button,
            this.keys02Button,
            this.drums01Button,
            this.drums02Button});
            this.drawingToolStrip.Location = new System.Drawing.Point(0, 64);
            this.drawingToolStrip.Name = "drawingToolStrip";
            this.drawingToolStrip.Padding = new System.Windows.Forms.Padding(0, 8, 1, 0);
            this.drawingToolStrip.Size = new System.Drawing.Size(93, 824);
            this.drawingToolStrip.TabIndex = 3;
            this.drawingToolStrip.Text = "toolStrip1";
            // 
            // pointerButton
            // 
            this.pointerButton.AutoSize = false;
            this.pointerButton.CheckOnClick = true;
            this.pointerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pointerButton.Image = ((System.Drawing.Image)(resources.GetObject("pointerButton.Image")));
            this.pointerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pointerButton.Name = "pointerButton";
            this.pointerButton.Size = new System.Drawing.Size(61, 50);
            this.pointerButton.Text = "pointerButton";
            this.pointerButton.Click += new System.EventHandler(this.pointerButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.AutoSize = false;
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(90, 6);
            // 
            // scaleLabel
            // 
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(91, 15);
            this.scaleLabel.Text = "Scale (.01 to 99)";
            // 
            // scale
            // 
            this.scale.Name = "scale";
            this.scale.Size = new System.Drawing.Size(89, 23);
            this.scale.Leave += new System.EventHandler(this.scale_Leave);
            this.scale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.scale_KeyDown);
            this.scale.TextChanged += new System.EventHandler(this.scale_TextChanged);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(91, 6);
            // 
            // singer01Button
            // 
            this.singer01Button.AutoSize = false;
            this.singer01Button.CheckOnClick = true;
            this.singer01Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.singer01Button.Image = ((System.Drawing.Image)(resources.GetObject("singer01Button.Image")));
            this.singer01Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.singer01Button.Name = "singer01Button";
            this.singer01Button.Size = new System.Drawing.Size(91, 68);
            this.singer01Button.Text = "Singer-01";
            this.singer01Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // singer02Button
            // 
            this.singer02Button.AutoSize = false;
            this.singer02Button.CheckOnClick = true;
            this.singer02Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.singer02Button.Image = ((System.Drawing.Image)(resources.GetObject("singer02Button.Image")));
            this.singer02Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.singer02Button.Name = "singer02Button";
            this.singer02Button.Size = new System.Drawing.Size(91, 68);
            this.singer02Button.Text = "Singer-02";
            this.singer02Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // guitarist01Button
            // 
            this.guitarist01Button.AutoSize = false;
            this.guitarist01Button.CheckOnClick = true;
            this.guitarist01Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.guitarist01Button.Image = ((System.Drawing.Image)(resources.GetObject("guitarist01Button.Image")));
            this.guitarist01Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.guitarist01Button.Name = "guitarist01Button";
            this.guitarist01Button.Size = new System.Drawing.Size(91, 68);
            this.guitarist01Button.Text = "Guitar-01";
            this.guitarist01Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // guitarist02Button
            // 
            this.guitarist02Button.AutoSize = false;
            this.guitarist02Button.CheckOnClick = true;
            this.guitarist02Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.guitarist02Button.Image = ((System.Drawing.Image)(resources.GetObject("guitarist02Button.Image")));
            this.guitarist02Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.guitarist02Button.Name = "guitarist02Button";
            this.guitarist02Button.Size = new System.Drawing.Size(91, 68);
            this.guitarist02Button.Text = "Guitar-02";
            this.guitarist02Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // bassist01Button
            // 
            this.bassist01Button.AutoSize = false;
            this.bassist01Button.CheckOnClick = true;
            this.bassist01Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bassist01Button.Image = ((System.Drawing.Image)(resources.GetObject("bassist01Button.Image")));
            this.bassist01Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bassist01Button.Name = "bassist01Button";
            this.bassist01Button.Size = new System.Drawing.Size(91, 68);
            this.bassist01Button.Text = "Bassist-01";
            this.bassist01Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // bassist02Button
            // 
            this.bassist02Button.AutoSize = false;
            this.bassist02Button.CheckOnClick = true;
            this.bassist02Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bassist02Button.Image = ((System.Drawing.Image)(resources.GetObject("bassist02Button.Image")));
            this.bassist02Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.bassist02Button.Name = "bassist02Button";
            this.bassist02Button.Size = new System.Drawing.Size(91, 68);
            this.bassist02Button.Text = "Bassist-02";
            this.bassist02Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // keys01Button
            // 
            this.keys01Button.AutoSize = false;
            this.keys01Button.CheckOnClick = true;
            this.keys01Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.keys01Button.Image = ((System.Drawing.Image)(resources.GetObject("keys01Button.Image")));
            this.keys01Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.keys01Button.Name = "keys01Button";
            this.keys01Button.Size = new System.Drawing.Size(91, 68);
            this.keys01Button.Text = "Keyboard-01";
            this.keys01Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // keys02Button
            // 
            this.keys02Button.AutoSize = false;
            this.keys02Button.CheckOnClick = true;
            this.keys02Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.keys02Button.Image = ((System.Drawing.Image)(resources.GetObject("keys02Button.Image")));
            this.keys02Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.keys02Button.Name = "keys02Button";
            this.keys02Button.Size = new System.Drawing.Size(91, 68);
            this.keys02Button.Text = "Keyboard-02";
            this.keys02Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // drums01Button
            // 
            this.drums01Button.AutoSize = false;
            this.drums01Button.CheckOnClick = true;
            this.drums01Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.drums01Button.Image = ((System.Drawing.Image)(resources.GetObject("drums01Button.Image")));
            this.drums01Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.drums01Button.Name = "drums01Button";
            this.drums01Button.Size = new System.Drawing.Size(91, 68);
            this.drums01Button.Text = "Drummer-01";
            this.drums01Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // drums02Button
            // 
            this.drums02Button.AutoSize = false;
            this.drums02Button.CheckOnClick = true;
            this.drums02Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.drums02Button.Image = ((System.Drawing.Image)(resources.GetObject("drums02Button.Image")));
            this.drums02Button.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.drums02Button.Name = "drums02Button";
            this.drums02Button.Size = new System.Drawing.Size(91, 68);
            this.drums02Button.Text = "Drummer-02";
            this.drums02Button.Click += new System.EventHandler(this.performerButton_Click);
            // 
            // drawingPanel
            // 
            this.drawingPanel.BackColor = System.Drawing.Color.Black;
            this.drawingPanel.Location = new System.Drawing.Point(97, 64);
            this.drawingPanel.Name = "drawingPanel";
            this.drawingPanel.Size = new System.Drawing.Size(817, 953);
            this.drawingPanel.TabIndex = 4;
            this.drawingPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.drawingPanel_MouseUp);
            // 
            // refreshTimer
            // 
            this.refreshTimer.Tick += new System.EventHandler(this.refreshTimer_Tick);
            // 
            // copyButton
            // 
            this.copyButton.AutoSize = false;
            this.copyButton.BackColor = System.Drawing.Color.Chartreuse;
            this.copyButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.copyButton.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copyButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(61, 61);
            this.copyButton.Text = "Copy";
            this.copyButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 888);
            this.Controls.Add(this.drawingPanel);
            this.Controls.Add(this.drawingToolStrip);
            this.Controls.Add(this.fileToolStrip);
            this.Name = "MainForm";
            this.Text = "Concert Drawing";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.fileToolStrip.ResumeLayout(false);
            this.fileToolStrip.PerformLayout();
            this.drawingToolStrip.ResumeLayout(false);
            this.drawingToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip fileToolStrip;
        private System.Windows.Forms.ToolStripButton newButton;
        private System.Windows.Forms.ToolStripButton openButton;
        private System.Windows.Forms.ToolStripButton saveButton;
        private System.Windows.Forms.ToolStrip drawingToolStrip;
        private System.Windows.Forms.ToolStripButton pointerButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel scaleLabel;
        private System.Windows.Forms.ToolStripTextBox scale;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton singer01Button;
        private System.Windows.Forms.ToolStripButton singer02Button;
        private System.Windows.Forms.ToolStripButton guitarist01Button;
        private System.Windows.Forms.ToolStripButton guitarist02Button;
        private System.Windows.Forms.ToolStripButton bassist01Button;
        private System.Windows.Forms.ToolStripButton bassist02Button;
        private System.Windows.Forms.ToolStripButton keys01Button;
        private System.Windows.Forms.ToolStripButton keys02Button;
        private System.Windows.Forms.ToolStripButton drums01Button;
        private System.Windows.Forms.ToolStripButton drums02Button;
        private System.Windows.Forms.Panel drawingPanel;
        private System.Windows.Forms.Timer refreshTimer;
        private System.Windows.Forms.ToolStripButton copyButton;
    }
}

