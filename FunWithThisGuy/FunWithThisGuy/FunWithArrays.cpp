#include <iostream>
#include <string>

using namespace std;

const int MY_INT_ARRAY_SIZE = 20;

void swapInts(int &a, int &b)
{
    int temp = a;
    a = b;
    b = temp;
}

int main(void)
{
    // TODO: write some fun code for these peeps, amiright?
    // TODO: Arrays
    
    // How we can explicitly define an array
    int myIntArray[MY_INT_ARRAY_SIZE] = { 4, 2, 6, 7, 5, 78, 100, 34, 21, 9 };
    
    // How we can assign random values to an array
    for (int i = 0; i < MY_INT_ARRAY_SIZE; i++)
    {
        myIntArray[i] = rand() % 1000;
    }

    // How we can print out values in our array
    for (int i = 0; i < MY_INT_ARRAY_SIZE; i++)
    {
        cout << "Value at " << i << ": " << myIntArray[i] << endl;
    }
    cout << "===========" << endl;

    // very basic approach to the bubble sort
    for (int j = 0; j < MY_INT_ARRAY_SIZE; j++)
    {
        for (int i = 1; i < MY_INT_ARRAY_SIZE; i++)
        {
            if (myIntArray[i - 1] > myIntArray[i])
            {
                swapInts(myIntArray[i - 1], myIntArray[i]);
            }
        }
    }

    for (int i = 0; i < MY_INT_ARRAY_SIZE; i++)
    {
        cout << "Value at " << i << ": " << myIntArray[i] << endl;
    }

    return 0;
}